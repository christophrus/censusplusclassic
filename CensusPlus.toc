## Interface: 70300
## Version: 7.0.
## X-SubVersion: >=WoWL7.3.5
## Title: CensusPlus
## Notes: This AddOn is licenced under the GNU GPL, see GPL.txt for details.
## Notes: Collects and displays census information. 
## Original Author: Ian Pieragostini
## Modified By: Cooper Sellers - www.warcraftrealms.com 
## Modified By: sylvanaar - LibWho integration
## Modified By: Bringoutyourdead with support from Balgair both at www.warcraftrealms.com 
## Modified By: Bringoutyourdead with support from Padanfain both at www.warcraftrealms.com 
## OptionalDeps: LibWho-2.0, LibStub, CallbackHandler-1.0
## SavedVariables: CensusPlus_Database, CensusPlus_CRealms, CensusPlus_Guilds, CensusPlus_JobQueue
## SavedVariablesPerCharacter: CensusPlus_PerCharInfo
## X-Website: http://www.warcraftrealms.com

#@no-lib-strip@
libs\LibStub\LibStub.lua
libs\CallbackHandler-1.0\CallbackHandler-1.0.lua
libs\LibWho-2.0\LibWho-2.0.lua
libs\LibRealmID\LibRealmID.lua
#@end-no-lib-strip@

CensusPlus.xml
CensusButton.xml
CensusPlayerList.xml
